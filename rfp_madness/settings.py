"""

RFP Madness Settings
====================

This is the configuration file for the RFP Madness project, which
follows the Django 'settings.py' standard approach.

Tip: Search this file for "# ReplaceMe" for the specific items
     you will need to modify.

Environment Variable Requirement
--------------------------------

You will probably need to create an environment variable to point
to this module. Here is how it would look in the bash shell:

    export DJANGO_SETTINGS_MODULE=rfp_madness.settings


Configuring Paths for Your Local System
---------------------------------------

This settings.py module contains some hard-coded paths and other attributes
specific to locale, system environment, etc.

In particular, you will definitely need to edit the following attributes:

This includes:

    DATABASES
    TEMPLATE_DIRS


Other Settings Which Could Bite You
-----------------------------------
It's also a good idea to scan this file for the other attributes which
might be relevant to your installation, such as TIME_ZONE.


Disabling Debug Settings (for non-developers)
---------------------------------------------

If you are an RFP Madness user and not attempting to debug/develop this
project, you should disable all settings related to debugging.

1. Disable the main Django DEBUG setting. This is near the top of the
file; just change True to False.

    DEBUG = False

2. Disable the django-debug-toolbar configuration. This is only needed
for development purposes. To disable it, comment out lines containing
the following:

    'debug_toolbar.middleware.DebugToolbarMiddleware'

Also, comment out or delete the entire django-debug-toolbar settings section.


"""

import os


DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.

        # ReplaceMe with the path where you want to keep your RFP Madness db
        'NAME': '/home/ballen/work/env/rfp_madness/db/devdb.db', #path to database file if using sqlite3.

        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.

# ReplaceMe with the timezone for your own RFP Madness!
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '5l8+@)j!f_66yiow$)172cs0e$w9lu($hwkzeq+q-sq+o(dlm1'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'rfp_madness.urls'

LOGIN_URL = '/accounts/login/'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'rfp_madness.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.

    # ReplaceMe -- modify with your installed path to RFP Madness
    '/home/ballen/work/env/rfp_madness/rfp_madness/templates',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',
    'debug_toolbar',
    'rfp_madness.kbase'
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
                    'require_debug_false': {
                        '()': 'django.utils.log.RequireDebugFalse'
                    }
               },

    'formatters': {
        'verbose': {
                  'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
                   },
        'simple': {
                  'format': '%(levelname)s %(message)s'
                  },
        },

    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
                       },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'simple'
                  },
                },

    'loggers': {
                    'django.request': {
                                        'handlers': ['mail_admins'],
                                        'level': 'ERROR',
                                        'propagate': True,
                                      },

                    'root': {
                                     'handlers': ['console',],
                                     'level': 'DEBUG',
                                   }
               },

    }



# Tests Discovery Section, inspired by PyCon 2012 presentation
# "Testing and Django" by Carl Meyer:
# http://pyvideo.org/video/699/testing-and-django
# See the README document for more info on rationale for using
# the defined TEST_RUNNER.

TEST_DISCOVER_TOP_LEVEL = os.path.dirname(os.path.dirname(__file__))
TEST_DISCOVER_ROOT = os.path.join(TEST_DISCOVER_TOP_LEVEL, 'tests')

TEST_RUNNER = 'discover_runner.DiscoverRunner'


### django-debug-toolbar settings ########################################

INTERNAL_IPS = ('127.0.0.1',)

def custom_show_toolbar(request):
    return True # Always show toolbar, for example purposes only.

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    'SHOW_TOOLBAR_CALLBACK': custom_show_toolbar,
#    'EXTRA_SIGNALS': ['myproject.signals.MySignal'],
#    'HIDE_DJANGO_SQL': False,
    'TAG': 'div',
    'ENABLE_STACKTRACES' : True,
    }

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
    )

##########################################################################