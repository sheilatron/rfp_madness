

from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()


urlpatterns = patterns('rfp_madness.kbase.views',
    url(r'^$', 'index'),
    url(r'^rfps/(?P<rfp_id>\d+)/$', 'rfp_form'),
    url(r'^rfps/add/$', 'add_rfp_form'),
    url(r'^rfps/add/form_submit/$', 'add_rfp_action'),
    url(r'^rfps/(?P<rfp_id>\d+)/form_submit/$', 'modify_rfp_action'),
)
