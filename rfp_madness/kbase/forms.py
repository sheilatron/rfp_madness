
from django.forms import ModelForm

import rfp_madness.kbase.models as kbmodel

class RfpForm(ModelForm):
    """
    Build an HTML form based on the RFP model.
    """
    class Meta:
        model = kbmodel.Rfp


class RfpDocumentForm(ModelForm):
    """
    Build an HTML form based on the RFP Document model.
    """
    class Meta:
        model = kbmodel.RfpDocument
