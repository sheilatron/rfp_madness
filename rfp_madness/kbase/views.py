
import logging

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotAllowed
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

import rfp_madness.kbase.models as kbmodel
import rfp_madness.kbase.forms as kbform

logger = logging.getLogger()

@login_required()
def index(request):
    """
    List up to 10 of the most recently requested RFPs, each linkable
    to the details of that RFP.
    """
    logger.debug('Calling kbase main view')
    print "Calling main kbase view...from a print statement."
    rfp_list = kbmodel.Rfp.objects.all().order_by('-request_dt')[:10]
    return render_to_response('kbase/index.html', {'rfp_list': rfp_list})

@login_required()
def rfp_form (request, rfp_id):
    logger.debug('Calling rfp_form view with id %s' % rfp_id)
    rfp = get_object_or_404(kbmodel.Rfp, pk=rfp_id)
    rfp_form = kbform.RfpForm(instance=rfp)
    return render_to_response('kbase/rfp.html', {'rfp': rfp,
                                                 'rfp_form': rfp_form},
        # need context to support csrf_token for
        # security reasons
        context_instance=RequestContext(request))

@login_required()
def modify_rfp_action (request, rfp_id):
    """
    Accept a POST to modify an existing RFP, and redirect to the RFP screen."
    """
    if request.method == 'POST':
        rfp = get_object_or_404(kbmodel.Rfp, pk=rfp_id)
        form = kbform.RfpForm(request.POST, instance=rfp)
        form.save() # will raise exception if form validation does not pass
        return HttpResponseRedirect(reverse('rfp_madness.kbase.views.rfp_form',
            args=(rfp_id,)
        ) # Redirect after POST
        )
    else:
        raise HttpResponseNotAllowed #TODO redirect

@login_required()
def add_rfp_form (request):
    """
    Form for entering a new RFP record.
    """
    rfp_form = kbform.RfpForm()
    rfp = kbmodel.Rfp()
    return render_to_response('kbase/rfp.html', {'rfp': rfp,
                                                 'rfp_form': rfp_form},
                               # need context to support csrf_token for
                               # security reasons
                               context_instance=RequestContext(request)
                             )

@login_required()
def add_rfp_action (request):
    """
    Capture form data for creating a new RFP record.
    """
    if request.method == 'POST':
        rfp = kbmodel.Rfp()
        form = kbform.RfpForm(request.POST, instance=rfp)
        form.save()
        return HttpResponseRedirect(reverse('rfp_madness.kbase.views.rfp_form',
                                            args=(rfp.id,),
                                           ),
                                    )
    else:
        raise HttpResponseNotAllowed #TODO redirect
