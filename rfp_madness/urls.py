
from django.conf.urls import patterns, include, url

# next two lines to enable the Django admin UI:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
    url(r'^accounts/profile/$', 'django.contrib.auth.views.login'), #TODO fix kludgy workaround
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout'),
    url(r'^$', 'rfp_madness.kbase.views.index'),
    url(r'^kbase/', include('rfp_madness.kbase.urls')),
    url(r'^admin/', include(admin.site.urls)),
)


