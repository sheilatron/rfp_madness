"""
Tests for model classes which are part of
"""

# TODO use factory_boy to go beyond trivial tests for models

from unittest import TestCase

from rfp_madness.kbase.models import Rfp, RfpDocument, RfpAnswer, RfpQuestion
import rfp_madness.testing_support.model_factories as mf


class TestRfp(TestCase):

    def test_instantiate (self):
        rfp = Rfp()

    def test_unicode(self):
        prospect = mf.ProspectFactory.build(prospect_name='Glurg')
        rfp = mf.RfpFactory.build(title='want Answers!', prospect=prospect)
        self.assertEquals(unicode(rfp), 'want Answers!')


class TestProspect (TestCase):
    def test_unicode (self):
        prospect = mf.ProspectFactory.build(prospect_name='Blammo')
        self.assertEqual(unicode(prospect), 'Blammo')
