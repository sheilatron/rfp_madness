

from unittest import TestCase


from rfp_madness.testing_support.model_factories import (SubjectCategoryFactory,)

class TestSubjectMatter(TestCase):

    def test_unicode (self):
        sc = SubjectCategoryFactory.build(category_name='Insectoid')
        assert 'Insectoid' in unicode(sc)

